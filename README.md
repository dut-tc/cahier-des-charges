# ![logo](cahier-des-charges.png) **[Cahier des charges](https://prezi.com/view/DibuvoCHTdp9Eo90LuWX/)**

Un cahier des charges (abréviation CDC), scope statement en anglais, est un document qui permet à un donneur d’ordre de faire savoir à un exécutant ce qui est attendu comme travail lors de la réalisation d’un projet.

## **A quoi sert un cahier des charges ? Quel est son rôle ? Quel objectif ?**

Le cahier des charges d’un site internet a deux utilités principales: 

Il permet à la personne qui va développer le site de comprendre les enjeux liés à celui-ci ainsi que le résultat attendu.
Son rôle est donc de permettre de comprendre le contexte, la typologie de site attendu ainsi que les fonctionnalités primaires.

La rédaction d’un cahier des charges permet de se poser les bonnes questions sur ce que l'on veut vraiment pour son site internet. 

Il permet de formaliser, sur du papier, les contours du site qui est attendu. 

Lister les fonctionnalités les plus importantes qui doivent être développées permet de faire un point sur l’objectif réel que l'on veut se donner avec son site.

Ainsi, un cahier des charges sert principalement à définir les contours d’un projet.

## **Que contient un cahier des charges ?**

+ **Présentation détaillée de votre entreprise et du contexte du projet**
+ **Présentation du contexte technique**
+ **Présentation des  fonctionnalités attendues**
+ **Présentation des modalités de sélection du prestataire**


## **[Un modèle de cahier des charges](Exemple-cahier-des-charges.docx)**